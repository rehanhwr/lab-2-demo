from django.apps import AppConfig


class Lab2DemoAddonConfig(AppConfig):
    name = 'lab_2_demo_addon'
